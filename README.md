A basic config file for Awesome WM.
Some widgets included but unused for now.

# Using config file

The files must be linked to ~/.config/awesome/ :

``` bash
$ mkdir -p ~/.config/awesome
$ ln rc.lua ~/.config/awesome
$ ln autorun.sh ~/.config/awesome
$ ln -s ~/Documents/awesome-config-files/widgets ~/.config/awesome
```
