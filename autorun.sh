#!/usr/bin/env bash

function run {
	if ! pgrep -f $1 ;
	then
		$@&
	fi
}

xrdb -merge ~/.Xressources

#compton
compton -b --backend glx --glx-no-stencil --vsync opengl-swc
